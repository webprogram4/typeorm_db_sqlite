import { AppDataSource } from "./data-source"
import { Product } from "./entity/Products"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const productRepository = AppDataSource.getRepository(Product)

    console.log("Loading users from the database...")
    const products = await productRepository.find()
    console.log("Loaded product: ", products)

    const updatedProduct = await productRepository.findOneBy({id: 1})
    console.log(updatedProduct)
    updatedProduct.price=80
    await productRepository.save(updatedProduct)

}).catch(error => console.log(error))
